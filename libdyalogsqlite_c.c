/** 
 * ----------------------------------------------------------------
 * $Id$
 * Copyright (C) 2003, 2004, 2005, 2009, 2011, 2012, 2013 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  libdyalogsqlite_c.c 
 *  \brief Support for libdyalogsqlite.pl
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
//#include <unistd.h>
#include <sqlite3.h>

#include "libdyalog.h"
#include "builtins.h"

sqlite3 *
DyALog_sqlite3_open(char *file) 
{
    int rc;
    sqlite3 *db;
    rc = sqlite3_open(file, &db);
    if( rc ){
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(1);
    }
    return db;
}

sqlite3 *
DyALog_sqlite3_open_readonly(char *file) 
{
    int rc;
    sqlite3 *db;
    rc = sqlite3_open_v2(file, &db,SQLITE_OPEN_READONLY,"unix-none");
    if( rc ){
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(1);
    }
    return db;
}


sqlite3_stmt *
DyALog_sqlite3_prepare(sqlite3 *db,const char *stmt) 
{
    sqlite3_stmt *pStmt;
    const char *zTail;
    int rc;
    rc = sqlite3_prepare(db,stmt,-1,&pStmt,&zTail);
    if (rc) {
        fprintf(stderr, "Can't prepare stmt: %s\n",stmt);
        exit(1);
    }
    return pStmt;
}

Bool
DyALog_sqlite3_getvalue (sqlite3_stmt *pstmt,
                        int column,
                        sfol_t value
                        )
{
    int type = sqlite3_column_type(pstmt,column);

    switch ((int) type) {

        case SQLITE_INTEGER:
            return Unify( DFOLINT(sqlite3_column_int(pstmt,column)), Key0,
                          value->t,value->k);
            break;
            
        case SQLITE_FLOAT:
            return Unify( DFOLFLT(sqlite3_column_double(pstmt,column)), Key0,
                          value->t,value->k);
            break;
            
        case SQLITE_TEXT:
        case SQLITE_BLOB:
            return Unify( Create_Atom(strdup(sqlite3_column_text(pstmt,column))), Key0,
                          value->t,value->k);
            break;
            
        case SQLITE_NULL:                /* we return a null list */
            return Unify( FOLNIL, Key0,
                          value->t, value->k );
            break;
    }
}


Bool
DyALog_sqlite3_bindvalues( sqlite3_stmt *pstmt,
                           sfol_t values
                           ) 
{
    int n = sqlite3_bind_parameter_count(pstmt);
    int i;
    Sdecl(x);
    for(i=1; i <= n; i++ ) {
        SFOL_Deref(values);
        if (!FOLPAIRP(values->t))
            Fail;
        x=FOLPAIR_CAR(values->t);
        Sk(x)=values->k;
        values->t=FOLPAIR_CDR(values->t);
        Deref(x);
//        dyalog_printf("bind value %d %x %&f\n",i,x,x);
        if (FOLSMBP(x)) {
            sqlite3_bind_text(pstmt,i,FOLSMB_NAME(x),-1,SQLITE_STATIC);
        } else if (FOLINTP(x)) {
            sqlite3_bind_int(pstmt,i,(int)CFOLINT(x));
        } else if (FOLFLTP(x)) {
            sqlite3_bind_double(pstmt,i,(double)CFOLFLT(x));
        } else if (FOLNILP(x)) {
            sqlite3_bind_null(pstmt,i);
        } else {
            Fail;
        }
    }
    Succeed;
}

Bool
DyALog_sqlite3_reset_and_bindvalues( sqlite3_stmt *pstmt,
                                     sfol_t values
                                     ) 
{
    sqlite3_reset(pstmt);
    DyALog_sqlite3_bindvalues(pstmt,values);
    Succeed;
}

fol_t
DyALog_sqlite3_getvalues( sqlite3_stmt *pstmt ) 
{
    fol_t res=FOLNIL;
    unsigned long l = sqlite3_data_count(pstmt);
    for(; l ; ) {
        int type = sqlite3_column_type(pstmt,--l);
        fol_t x;
        
        switch ((int) type) {
            
            case SQLITE_INTEGER:
                x=DFOLINT(sqlite3_column_int(pstmt,l));
                break;
                
            case SQLITE_FLOAT:
                x=DFOLFLT(sqlite3_column_double(pstmt,l));
                break;
                
            case SQLITE_TEXT:
            case SQLITE_BLOB:
                x = Create_Atom(strdup(sqlite3_column_text(pstmt,l)));
                break;
                
            case SQLITE_NULL:                /* we return a null list */
                x = FOLNIL;
                break;
        }
        res = Create_Pair(x,res);
    }
    return res;
}

Bool
DyALog_sqlite3_tuple( sqlite3_stmt *pstmt,
                      sfol_t row
                      ) 
{
  retry:
    switch (sqlite3_step(pstmt)) {
        case 0:                 /* ok */
//            dyalog_printf("status ok\n");
            Succeed;
            break;
        case 5:                 /* busy */
//            dyalog_printf("status busy\n");
            goto retry;
            break;
        case 100:                 /* row available */
//            dyalog_printf("status row\n");
            return Unify(DyALog_sqlite3_getvalues(pstmt),Key0,row->t,row->k);
            break;
        case 101:                 /* done */
//            dyalog_printf("status done\n");
            No_More_Choice();
            return Unify(Create_Atom("done"),Key0,row->t,row->k);
            break;
        default:                /* all kinds of error */
//            dyalog_printf("status error\n");
            No_More_Choice();
            Fail;
            break;
    }
}


Bool
DyALog_sqlite3_first_tuple( sqlite3_stmt *pstmt,
                            sfol_t row
                            ) 
{
  retry:
    switch (sqlite3_step(pstmt)) {
        case 0:                 /* ok */
//            dyalog_printf("status ok\n");
            Succeed;
            break;
        case 5:                 /* busy */
//            dyalog_printf("status busy\n");
            goto retry;
            break;
        case 100:                 /* row available */
//            dyalog_printf("status row\n");
            return Unify(DyALog_sqlite3_getvalues(pstmt),Key0,row->t,row->k);
            break;
        default:
            No_More_Choice();
            Fail;
            break;
    }
}


        
