/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2005, 2011, 2013 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  libdyalogsqlite.pl -- DyALog wrapper for SQLITE 3 API
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % Interface
 
:-module(sqlite).

:-import{ file=>'format.pl', preds => [format/2,format_hook/4,format/3,error/2] }.

:-std_prolog
	errmsg/1,
	open/2,
	open_readonly/2,
	close/1,
	prepare/3,
	step/2,
	finalize/1,
	exec/4,
	getvalue/3,
	getvalues/2,
	bind/2,
	reset_and_bind/2,
	tuple/2,
	first_tuple/2,
	changes/2,
%%	clear_bindings/1,
	column_count/2,
	column_name/3,
	column_names/2,
	data_count/2,
%%	db_handle/2,
	last_insert_rowid/2,
	reset/1
	.

:-light_tabular version/1.

:-end_require.

%% End of Interface
%% The code below is not read when the file is imported

:-finite_set(sqlite!status,
	     [sqlite!ok,
	      sqlite!error,
	      sqlite!internal,
	      sqlite!perm,
	      sqlite!abort,
	      sqlite!busy,
	      sqlite!locked,
	      sqlite!nomem,
	      sqlite!readonly,
	      sqlite!interrupt,
	      sqlite!ioerr,
	      sqlite!corrupt,
	      sqlite!notfound,
	      sqlite!full,
	      sqlite!cantopen,
	      sqlite!protocol,
	      sqlite!empty,
	      sqlite!schema,
	      sqlite!toobig,
	      sqlite!constraint,
	      sqlite!mismatch,
	      sqlite!misuse,
	      sqlite!nolfs,
	      sqlite!auth,
	      sqlite!row,
	      sqlite!done
	     ]).

:-extensional sqlite!status/2.

sqlite!status(0,sqlite!ok).    /* Successful result */
sqlite!status(1,sqlite!error).    /* SQL error or missing database */
sqlite!status(2,sqlite!internal).    /* An internal logic error in SQLite */
sqlite!status(3,sqlite!perm).    /* Access permission denied */
sqlite!status(4,sqlite!abort).    /* Callback routine requested an abort */
sqlite!status(5,sqlite!busy).    /* The database file is locked */
sqlite!status(6,sqlite!locked).    /* A table in the database is locked */
sqlite!status(7,sqlite!nomem).    /* A malloc() failed */
sqlite!status(8,sqlite!readonly).    /* Attempt to write a readonly database */
sqlite!status(9,sqlite!interrupt).    /* Operation terminated by sqlite_interrupt() */
sqlite!status(10,sqlite!ioerr).    /* Some kind of disk I/O error occurred */
sqlite!status(11,sqlite!corrupt).    /* The database disk image is malformed */
sqlite!status(12,sqlite!notfound).    /* (Internal Only) Table or record not found */
sqlite!status(13,sqlite!full).    /* Insertion failed because database is full */
sqlite!status(14,sqlite!cantopen).    /* Unable to open the database file */
sqlite!status(15,sqlite!protocol).    /* Database lock protocol error */
sqlite!status(16,sqlite!empty).    /* (Internal Only) Database table is empty */
sqlite!status(17,sqlite!schema).    /* The database schema changed */
sqlite!status(18,sqlite!toobig).    /* Too much data for one row of a table */
sqlite!status(19,sqlite!constraint).    /* Abort due to constraint violation */
sqlite!status(20,sqlite!mismatch).    /* Data type mismatch */
sqlite!status(21,sqlite!misuse).    /* Library used incorrectly */
sqlite!status(22,sqlite!nolfs).    /* Uses OS features not supported on host */
sqlite!status(23,sqlite!auth).    /* Authorization denied */
sqlite!status(100,sqlite!row).   /* sqlite_step() has another row ready */
sqlite!status(101,sqlite!done).   /* sqlite_step() has finished executing */


:-finite_set(sqlite!type,
	     [sqlite!int,
	      sqlite!float,
	      sqlite!text,
	      sqlite!blob,
	      sqlite!null
	     ]).

:-extensional sqlite!type/2.

sqlite!type(1,sqlite!int).
sqlite!type(2,sqlite!float).
sqlite!type(3,sqlite!text).
sqlite!type(4,sqlite!blob).
sqlite!type(5,sqlite!null).

errmsg(ErrN) :-
	'$interface'( sqlite3_errmsg( ErrN:int ), [return(Msg:string) ] ),
	error('~w',[Msg])
	.

%% Open a connection to a database
open(File,DB) :-
	'$interface'( 'DyALog_sqlite3_open'(File:string), [return(DB:ptr)] )
	.

%% Open a connection to a database
open_readonly(File,DB) :-
	'$interface'( 'DyALog_sqlite3_open_readonly'(File:string), [return(DB:ptr)] )
	.

%% Close a connection to a database
sqlite!close(DB) :-
	'$interface'( sqlite3_close(DB:ptr), [return(R:int)] ),
	( R == 0 xor errmsg(R) )
	.

%% Compile a statement
prepare(DB,Stmt,PStmt) :-
	'$interface'( 'DyALog_sqlite3_prepare'(DB:ptr,Stmt:string),[return(PStmt:ptr)])
	.

%% Advance in the execution of a prepared and bound statement
step(PStmt,R) :-
	'$interface'(sqlite3_step(PStmt:ptr),[return(R:int)])
	.

:-xcompiler
inline_step(PStmt,R) :-
	'$interface'(sqlite3_step(PStmt:ptr),[return(R:int)])
	.


%% Free a prepared statement
finalize(PStmt) :-
	'$interface'(sqlite3_finalize(PStmt:ptr),[return(R:int)]),
%%	( R == 0 xor errmsg(R) ),
	true
	.

exec(DB,Stmt,Bind,Row) :-
	prepare(DB,Stmt,PStmt),
	bind(PStmt,Bind),
	tuple(PStmt,Row),
	( Row == done -> finalize(PStmt) ; true ).

%% Get next row for a prepared and bound statement
% tuple(PStmt,Row) :-
% 	inline_step(PStmt,R),
% 	sqlite!status(R,S),
% 	tuple_handler(S,PStmt,Row)
% 	.

tuple(PStmt,Row) :-
	'$interface'('DyALog_sqlite3_tuple'(PStmt:ptr,Row:term),[choice_size(0)])
	.

first_tuple(PStmt,Row) :-
	'$interface'('DyALog_sqlite3_first_tuple'(PStmt:ptr,Row:term),[])
	.


:-rec_prolog tuple_handler/3.

tuple_handler(sqlite!busy,PStmt,Row) :-	
	%% Try again
	format('BUSY\n',[]),
	tuple(PStmt,Row)
	.

tuple_handler(sqlite!done,PStmt,done) :-
	%% format('DONE\n',[]),
	true
	.

tuple_handler(sqlite!ok,PStmt,Row) :-
	(   format('OK\n',[])
	;   
	    tuple(PStmt,Row)
	)
	.

tuple_handler(sqlite!row,PStmt,Row) :-
	(   getvalues(PStmt,Row)
	;   
	    tuple(PStmt,Row)
	)
	.

tuple_handler(sqlite!error,PStmt,Row) :-
	format('ERROR\n',[]),
	fail.

tuple_handler(sqlite!misuse,PStmt,Row) :-
	format('MISUSE\n',[]),
	fail.

%% Get current value for a given column in the current row
getvalue(PStmt,I,Value) :-
	'$interface'( 'DyALog_sqlite3_getvalue'(PStmt:ptr,I:int,Value:term),[])
	.

:-xcompiler
inline_getvalue(PStmt,I,Value) :-
	'$interface'( 'DyALog_sqlite3_getvalue'(PStmt:ptr,I:int,Value:term),[])
	.

%% Get number of columns in current row
data_count(PStmt,Length) :-
	'$interface'(sqlite3_data_count(PStmt:ptr),[return(Length:int)]).

%% Get current row of values
getvalues(PStmt,Values) :-
	'$interface'('sqlite3_data_count'(PStmt:ptr),[return(Length:int)]),
	mutable(M,[],true),     % backtrackable: disappear when exiting
	every((
	       '$interface'( 'DyALog_Count'(0:int,Length:int,I: -int),
			     [ choice_size(0) ]),
	       inline_getvalue(PStmt,I,Value),
	       mutable_list_extend(M,Value)
	       )),
	mutable_read(M,Values)
	.

%% Bind parameters to value for a prepared statement
bind(PStmt,Values) :-
	'$interface'('DyALog_sqlite3_bindvalues'(PStmt:ptr,Values:term),[])
	.

:-xcompiler
inline_bind(PStmt,Values) :-
	'$interface'('DyALog_sqlite3_bindvalues'(PStmt:ptr,Values:term),[])
	.


%% Return the number of changes performed by the last statement (insertion, update, deletion)
changes(DB,N) :-
	'$interface'(sqlite3_changes(DB:ptr),return(N:int))
	.

:-xcompiler
inline_changes(DB,N) :-
	'$interface'(sqlite3_changes(DB:ptr),return(N:int))
	.

%% Clear bindings
% clear_bindings(PStmt) :-
% 	'$interface'(sqlite3_clear_bindings(PStmt:ptr),[])
% 	.

%% Reset a prepared statement but keep bindings
reset(PStmt) :-
	'$interface'(sqlite3_reset(PStmt:ptr),[return(N:int)]),
	true
	.

:-xcompiler
inline_reset(PStmt) :-
	'$interface'(sqlite3_reset(PStmt:ptr),[return(N:int)]),
	true
	.

reset_and_bind(PStmt,Values) :-
	'$interface'('DyALog_sqlite3_reset_and_bindvalues'(PStmt:ptr,Values:term),[])
	.

%% Column count in prepared statement
column_count(PStmt,N) :-
	'$interface'(sqlite3_column_count(PStmt:ptr),[return(N:int)])
	.

%% Column name in prepared statement
column_name(PStmt,I,Name) :-
	'$interface'(sqlite3_column_name(PStmt:ptr,I:int),[return(Name:string)])
	.

%% Get the list of column names for a prepared statement
column_names(PStmt,Names) :-
	'$interface'('sqlite3_column_count'(PStmt:ptr),[return(Length:int)]),
	mutable(M,[],true),     % backtrackable: disappear when exiting
	(   '$interface'( 'DyALog_Count'(0:int,Length:int,I: -int),
			  [ choice_size(0) ]),
	    column_name(PStmt,I,Name),
	    mutable_read(M,L),
	    mutable(M,[Name|L]),
	    fail
	;   
	    true
	),
	mutable_read(M,Names)
	.

%% Retrieve Database from prepared statement
% db_handle(PStmt,DB) :-
% 	'$interface'(sqlite3_db_handle(PStmt:ptr),[return(DB:ptr)])
% 	.

%% Retrieve last inserted row id
last_insert_rowid(DB,Id) :-
	'$interface'(sqlite3_last_insert_rowid(DB:ptr),[return(Id:int)])
	.

%% Sqlite3 version
version(Version) :-
	'$interface'(sqlite3_libversion,[return(Version:string)]).
	

%% number of changes since opening database
total_changes(DB,N) :-
	'$interface'(sqlite3_total_changes(DB:ptr),[return(N:int)])
	.

:-xcompiler
mutable_list_extend(M,X) :- '$interface'('DyALog_Mutable_List_Extend'(M:ptr,X:term),[return(none)]).	
