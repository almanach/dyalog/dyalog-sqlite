/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2005 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  test.pl -- Small test file for libdyalogsqlite
 *
 * ----------------------------------------------------------------
 * Description
 *   A small sample file to test sqlite3 API in DyALog
 * ----------------------------------------------------------------
 */

 :-import{ module=> sqlite, 
	   file => 'libdyalogsqlite.pl',
	   preds => [
		     open/2,
		     close/1,
		     exec/4,
		     prepare/3,
		     tuple/2,
		     finalize/1,
		     column_names/2,
		     column_count/2,
		     reset/1,
		     bind/2
		     ]
	 }.


:-require('format.pl').

test :-
	argv([File]),
	open(File,DB),
	exec(DB,'create table toto (k TEXT,v INTEGER)',[],_R),
	format('I am here ~w\n',[_R]),
	exec(DB,'insert into toto(k,v) values (?,?)',[a,2],done),
	exec(DB,'insert into toto(k,v) values (?,?)',[a,3],done),
	exec(DB,'insert into toto(k,v) values (?,?)',[b,4],done),
	prepare(DB,'select * from toto where k=?',PStmt),
	bind(PStmt,[a]),
	column_count(PStmt,Length),
	column_names(PStmt,Names),
	format('Number of columns: ~w = ~w\n',[Length,Names]),
	format('Bound k to ~w\n',[a]),
	every((tuple(PStmt,X),
	       X \== done,
	       format('Got row ~w=~w\n',[Names,X]))),
%%	finalize(PStmt),
	sqlite!reset(PStmt),
	format('I am here 2\n',[]),
	bind(PStmt,[b]),
	format('Bound k to ~w\n',[b]),
	every((tuple(PStmt,X),
	       X \== done,
	       format('New Got row ~w=~w\n',[Names,X]))),
	finalize(PStmt),
	format('Destroyed prepared statement\n',[]),
	close(DB),
	format('I am here 3\n',[])
	.

?-test.
